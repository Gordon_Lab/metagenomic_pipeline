#!/bin/sh

# A default version of vars.sh (renamed config.sh) would be pulled down to analysis folder when running retrieve_sbatch.sh; user would then modify
# as needed before initiating analysis with metagenomic_pipeline.sbatch
# Structure below organizes parameters by step/stage/.sbatch; alternative approach would be to organize by variable type (paths, resource allocation, etc.)
	# Not sure yet which approach is more user-friendly
# All variables would be pre-defined with defaults where possible
	# Variables that must be defined by the user should be left empty in default file
	# A check would need to be performed at some point to be sure variables that must be user-defined are not emptyu
		# Probably best to do error-checking upfront (within metagenomic_pipeline.sh) rather than waiting until you get to the step where a variable is needed
# General guidelines/suggestions for setting resource allocations could be provided in comments at the end of this file

# User MUST set this variable
export MAP_FILE="231003_SHINE_mapping_file.txt"

# General (not step-specific)
export WORK_DIR=$( pwd )
export LOG_DIR=${WORK_DIR}/logs
export READS_DIR=${WORK_DIR}/raw_data
export SID_FILE=${WORK_DIR}/SID.list
export SUBGROUP_FILE=${WORK_DIR}/SUBGROUP.list
export IN_RUN_VARIABLES="in_run_variables.sh"

if ! [ -f "${WORK_DIR}/${IN_RUN_VARIABLES}" ]; then
    echo -e "#!/bin/sh\necho [VARIABLES] Runtime environment variables set." >> $IN_RUN_VARIABLES
    chmod 755 $IN_RUN_VARIABLES
fi

# Log directory structure is created up front, other directories are created as needed
mkdir -p $LOG_DIR
mkdir -p ${LOG_DIR}/file_handling
mkdir -p ${LOG_DIR}/trim
mkdir -p ${LOG_DIR}/hostfilter
mkdir -p ${LOG_DIR}/assembly
mkdir -p ${LOG_DIR}/annotation
mkdir -p ${LOG_DIR}/quantitation
mkdir -p ${LOG_DIR}/mag
mkdir -p ${LOG_DIR}/taxonomy

# Step 1 -- File renaming, adapter trimming, and read filtering [seq_file_renamer.sbatch, trim_galore_array.sbatch]
# This section is a little confusing b/c it covers two discrete steps/.sbatch files
export QC_DIR=${WORK_DIR}/01_quality_control
export RENAMING_MEM_ALLOC="200M"
export RENAMING_CPU_ALLOC=1
export TRIM_MEM_ALLOC="2G"
export TRIM_CPU_ALLOC=4
#export TRIM_SOFTWARE_V=0.6.4	# Might be best to lock specific tool versions to specific pipeline versions and leave this static?
# Any other trim_galore options the user might want to specify could be provided here; or, could provide all of them here to make it easier for user
# to see settings without tracking down; mapping variable names to options might get confusing though

# Step 2 -- Host read mapping [bowtie_host_filter_array.sbatch]
export HF_DIR=${WORK_DIR}/02_host_filtered
export HOSTFILTER_MEM_ALLOC="4G"				# I don't think this requirement will ever exceed ~4G when mapping to the human index
export HOSTFILTER_CPU_ALLOC=2
export HOST_DB_HUMAN="/ref/jglab/data/genomes/Homo_sapiens/UCSC/hg19/Sequence/Bowtie2Index/genome"
export HOST_DB_MOUSE="/ref/jglab/data/genomes/Mus_musculus/UCSC/mm10/Sequence/Bowtie2Index/genome"
export HOST_DB_PIG="/ref/jglab/data/genomes/Sus_scrofa/UCSC/susScr3/Sequence/Bowtie2Index/genome"

# Step 3 -- Contig assembly [megahit_array.sbatch]
export ASSEMBLY_DIR=${WORK_DIR}/03_metagenome_assembly
export ASSEMBLY_MEM_ALLOC="32G"
export ASSEMBLY_CPU_ALLOC=4
export ASSEMBLY_SUBGROUP_FLAG=true
export ASSEMBLY_SUBGROUP_MEM_ALLOC="64G"
export ASSEMBLY_SUBGROUP_CPU_ALLOC=8
# Any other megahit options the user might want to specify could be provided here; or, could provide all of them here to make it easier for user
# to see settings without tracking down; mapping variable names to options might get confusing though

# Step 4 -- Annotation [prokka_array.sbatch]
export ANNO_DIR=${WORK_DIR}/04_metagenome_annotation
export ANNO_MEM_ALLOC="4G"
export ANNO_CPU_ALLOC=2
export ANNO_TBL2ASN_FLAG=false	# Value dictates whether to run the lab-installed prokka version with ('true') or without NCBI's tbl2asn
export ANNO_RRNA_TOOL="BARRNAP"	# Alternatively, can switch to "RNAMMER" if user wants to (try to) run RNAmmer predictions for rRNA
# Any other prokka options the user might want to specify could be provided here; or, could provide all of them here to make it easier for user
# to see settings without tracking down; mapping variable names to options might get confusing though

# Step 5 -- Contig quantification
export CONTIG_COUNTS_DIR=${WORK_DIR}/05_contig_quantification
export KALLISTO_CONTIG_BAM_FLAG=false
export KALLISTO_CONTIG_INDEX_MEM_ALLOC="24G"
export KALLISTO_CONTIG_INDEX_CPU_ALLOC=1
export KALLISTO_CONTIG_QUANT_MEM_ALLOC="16G"
export KALLISTO_CONTIG_QUANT_CPU_ALLOC=4

# Step 6 -- MAG reconstruction
export BIN_DIR=${WORK_DIR}/06_contig_binning
export BINNING_MEM_ALLOC="1G"
export BINNING_CPU_ALLOC="4"

# Step 7 - MAG assembly reconciliation
export DAS_OUT=${WORK_DIR}/07_mag_assembly_reconciliation
export DAS_MEM_ALLOC="2G"
export DAS_CPU_ALLOC="4"
export DAS_ADD_METHODS="ON"

# Step 8 - MAG refinement
export MAG_REFINEMENT_DIR=${WORK_DIR}/08_MAG_refinement
export MAGPURIFY_MEM_ALLOC="2G"
export MAGPURIFY_CPU_ALLOC="1"
export MAGPURIFYDB="/ref/jglab/data/magpurify/1.0/"

# Step 9 - Dereplication
export DREP_DIR=${WORK_DIR}/09_dereplication
export CHECKM_MEM_ALLOC="48G"
export CHECKM_CPU_ALLOC="4"
export DREP_MEM_ALLOC="120G"
export DREP_CPU_ALLOC="16"
export DREP_COMPLETENESS="90"
export DREP_CONTAMINATION="5"
export DREP_PRIMARY_ANI="0.9"
export DREP_SECONDARY_ANI="0.99"

# Step 10 -- MAG annotation
export MAG_ANNO_DIR=${WORK_DIR}/10_mag_annotation
export MAG_ANNO_MEM_ALLOC="4G"
export MAG_ANNO_CPU_ALLOC=2
export MAG_ANNO_TBL2ASN_FLAG=true  # Value dictates whether to run the lab-installed prokka version with ('true') or without NCBI's
export MAG_ANNO_RRNA_TOOL="BARRNAP" # Alternatively, can switch to "RNAMMER" if user wants to (try to) run RNAmmer predictions for r
# Any other prokka options the user might want to specify could be provided here; or, could provide all of them here to make it easi
# to see settings without tracking down; mapping variable names to options might get confusing though
export MAG_ANNO_ANNO_FLAG=false # this flag turns on/off the functionality in prokka to attempt to apply annotations from reference
# databases to each ORF. This can be slow for thousands of MAGs and unnecessary/meaningless for metagenomes unless you need it for a
# specific purpose.

# Step 11 -- MAG Taxonomy
export MAG_TAX_DIR=${WORK_DIR}/11_mag_taxonomic_assignment
export GTDB_MEM_ALLOC="200G"
export GTDB_CPU_ALLOC="6"
export KRAKEN_RUN_FLAG=false
export KRAKEN_MEM_ALLOC="300G"
export KRAKEN_CPU_ALLOC="12"
export KRAKEN_DB="/ref/jglab/data/kraken_gtdb/release207/"
export BRACKEN_DB="/ref/jglab/data/kraken_gtdb/release207/"

# Step 12 -- MAG Quantitation
export MAG_COUNTS_DIR=${WORK_DIR}/12_mag_quantification
export KALLISTO_MAG_COLLAPSE_FLAG=false
export KALLISTO_MAG_INDEX_MEM_ALLOC="100G"
export KALLISTO_MAG_INDEX_CPU_ALLOC=12
export KALLISTO_MAG_QUANT_MEM_ALLOC="100G"
export KALLISTO_MAG_QUANT_CPU_ALLOC=4

echo "[VARIABLES] Environment variables set."
