#!/bin/bash

################################################################################
# SCRIPT NAME : metagenomic_pipeline.sbatch                                    #
# DESCRIPTION : pipeline submission template for metagenomic sequence analysis #
# ARGS        :                                                                #
# AUTHOR      : Matthew C. Hibberd                                             #
# EMAIL       : hibberdm@wustl.edu                                             #
# UPDATED     : November 6, 2023                                               #
################################################################################

## SHELL BEHAVIOR MODIFIERS
# 'set -e' causes immediate exit on non-zero status
set -e

## LOAD THE MODULE
eval $(spack load --sh metagenomic-pipeline@4.9.4)

## CONFIGURE THE ENVIRONMENT
# This step identifies the 'config.sh' file, sets default variables,
# checks the mapping file, and gathers basic info about how many
# samples and assemblies are planned for downstream array specification.

source configure_mgx_env.sh

echo "[STATUS] indexing contigs"
CONTIG_INDEX_JOB=$(sbatch \
  --mem=$KALLISTO_CONTIG_INDEX_MEM_ALLOC \
  --cpus-per-task=$KALLISTO_CONTIG_INDEX_CPU_ALLOC \
  -o "${LOG_DIR}/quantitation/slurm-kallisto_contig_index-%A_%a.out" \
  --array=1-"${ASSEMBLIES}"%100 \
  kallisto_contig_index.sbatch \
  "${ASSEMBLIES_FILE}" \
  $CONFIG_LOC \
  | awk '{ print $NF }')

echo "[STATUS] quantifying contigs"
CONTIG_QUANT_JOB=$(sbatch \
  -d afterok:"$CONTIG_INDEX_JOB" \
  --mem=$KALLISTO_CONTIG_QUANT_MEM_ALLOC \
  --cpus-per-task=$KALLISTO_CONTIG_QUANT_CPU_ALLOC \
  -o "${LOG_DIR}/quantitation/slurm-kallisto_contig_quantification-%A_%a.out" \
  --array=1-"${SAMPLES}"%100 \
  kallisto_contig_array.sbatch \
  "${SAMPLES_FILE}" \
  $CONFIG_LOC \
  | awk '{ print $NF }')

echo "[STATUS] processing contig quantification data"
CONTIG_QUANT_TABLES_JOB=$(sbatch \
  -d afterok:"$CONTIG_QUANT_JOB" \
  --mem=$BINNING_MEM_ALLOC \
  --cpus-per-task=1 \
  -o "${LOG_DIR}/quantitation/slurm-prep_contig_abundances_for_mag_assembly-%A_%a.out" \
  --array=1-"${ASSEMBLIES}"%100 \
  prep_contig_abundances_for_mag_assembly.sbatch \
  "${ASSEMBLIES_FILE}" \
  $CONFIG_LOC \
  | awk '{ print $NF }')

echo "[STATUS] beginning the binning process"
METABAT_JOB=$(sbatch \
  -d afterok:"$CONTIG_QUANT_TABLES_JOB" \
  --mem=$BINNING_MEM_ALLOC \
  --cpus-per-task=$BINNING_CPU_ALLOC \
  -o "${LOG_DIR}/mag/slurm-binning_metabat-%A_%a.out" \
  --array=1-"${ASSEMBLIES}"%100 \
  binning_metabat.sbatch \
  "${ASSEMBLIES_FILE}" \
  $CONFIG_LOC \
  | awk '{ print $NF }')

MAXBIN_JOB=$(sbatch \
  -d afterok:"$CONTIG_QUANT_TABLES_JOB" \
  --mem=$BINNING_MEM_ALLOC \
  --cpus-per-task=$BINNING_CPU_ALLOC \
  -o "${LOG_DIR}/mag/slurm-binning_maxbin-%A_%a.out" \
  --array=1-"${ASSEMBLIES}"%100 \
  binning_maxbin.sbatch \
  "${ASSEMBLIES_FILE}" \
  $CONFIG_LOC \
  | awk '{ print $NF }')

CONCOCT_JOB=$(sbatch \
  -d afterok:"$CONTIG_QUANT_TABLES_JOB" \
  --mem=$BINNING_MEM_ALLOC \
  --cpus-per-task=$BINNING_CPU_ALLOC \
  -o "${LOG_DIR}/mag/slurm-binning_concoct-%A_%a.out" \
  --array=1-"${ASSEMBLIES}"%100 \
  binning_concoct.sbatch \
  "${ASSEMBLIES_FILE}" \
  $CONFIG_LOC \
  | awk '{ print $NF }')

VAMB_JOB=$(sbatch \
  -d afterok:"$CONTIG_QUANT_TABLES_JOB" \
  --mem=$BINNING_MEM_ALLOC \
  --cpus-per-task=$BINNING_CPU_ALLOC \
  -o "${LOG_DIR}/mag/slurm-binning_vamb-%A_%a.out" \
  --array=1-"${ASSEMBLIES}"%100 \
  binning_vamb.sbatch \
  "${ASSEMBLIES_FILE}" \
  $CONFIG_LOC \
  | awk '{ print $NF }')

echo "[STATUS] reconciling multiple binning strategies"
DAS_JOB=$(sbatch \
  -d afterok:"$METABAT_JOB":"$MAXBIN_JOB":"$CONCOCT_JOB":"$VAMB_JOB" \
  --mem=$DAS_MEM_ALLOC \
  --cpus-per-task=$DAS_CPU_ALLOC \
  -o "${LOG_DIR}/mag/slurm-das_tool-%A_%a.out" \
  --array=1-"${ASSEMBLIES}"%100 \
  das_tool.sbatch \
  ${ASSEMBLIES_FILE} \
  ${CONFIG_LOC} \
  | awk '{ print $NF }')

echo "[STATUS] refining mags"
PREP_REFINE_JOB=$(sbatch \
  -d afterok:"$DAS_JOB" \
  --wait \
  -o "${LOG_DIR}/mag/slurm-get_mags_for_refinement-%A_%a.out" \
  get_mags_for_refinement.sbatch \
  ${CONFIG_LOC} \
  | awk '{ print $NF }')

wait

source RUNTIME_VARIABLES.sh

REFINE_JOB=$(sbatch \
  -d afterok:"$PREP_REFINE_JOB" \
  --mem=$MAGPURIFY_MEM_ALLOC \
  --cpus-per-task=$MAGPURIFY_CPU_ALLOC \
  -o "${LOG_DIR}/mag/slurm-magpurify-%A_%a.out" \
  --array=1-"${MAG_COUNT_FOR_REFINEMENT}"%100 \
  magpurify.sbatch \
  ${CONFIG_LOC} \
  | awk '{ print $NF }' )

echo "[STATUS] dereplicating"
if [ "$CHECKM_VERSION" == "checkm1" ]; then

  PREP_CHECKM_JOB=$(sbatch \
   -d afterok:"$REFINE_JOB" \
   --wait \ 
   -o "${LOG_DIR}/slurm-prep_checkm_job-%A.out" \
   prep_checkm.sbatch \
   ${CONFIG_LOC} \
   | awk '{ print $NF }' )
  
  wait

  source RUNTIME_VARIABLES.sh

  CHECKM_JOB=$(sbatch \
   -d afterok:"$PREP_CHECKM_JOB" \
   --mem=$CHECKM_MEM_ALLOC \
   --cpus-per-task=$CHECKM_CPU_ALLOC \
    -o "${LOG_DIR}/mag/slurm-checkm_mag_eval-%A_%a.out" \
   --array=1-"${CHECKM_BATCH_COUNT}"%100 \
   checkm_split_array.sbatch \
   ${CONFIG_LOC} \
   | awk '{ print $NF }' )

else

  CHECKM_JOB=$(sbatch \
   -d afterok:"$REFINE_JOB" \
   --mem=$CHECKM_MEM_ALLOC \
   --cpus-per-task=$CHECKM_CPU_ALLOC \
   -o "${LOG_DIR}/mag/slurm-checkm2_mag_eval-%A_%a.out" \
   checkm2_mag_evaluation.sbatch \
   ${CONFIG_LOC} \
   | awk '{ print $NF }' )

fi

DEREP_JOB=$(sbatch \
  -d afterok:"$CHECKM_JOB" \
  --mem=$DREP_MEM_ALLOC \
  --cpus-per-task=$DREP_CPU_ALLOC \
  -o "${LOG_DIR}/mag/slurm-drep-%A.out" \
  drep.sbatch \
  ${CONFIG_LOC} \
  | awk '{ print $NF }')

echo "[STATUS] annotating dereplicated mags"
PREP_ANNO_JOB=$(sbatch \
  -d afterok:"$DEREP_JOB" \
  --wait \
  -o "${LOG_DIR}/slurm-get_mags_for_annotation-%A.out" \
  get_mags_for_annotation.sbatch \
  ${CONFIG_LOC} \
  | awk '{ print $NF }')

wait

source RUNTIME_VARIABLES.sh

ANNO_JOB=$(sbatch \
  -d afterok:"$PREP_ANNO_JOB" \
  --mem=$MAG_ANNO_MEM_ALLOC \
  --cpus-per-task=$MAG_ANNO_CPU_ALLOC \
  -o "${LOG_DIR}/annotation/slurm-mag_annotation-%A_%a.out" \
  --array=1-"${MAG_COUNT_FOR_ANNOTATION}"%100 \
  prokka_mag.sbatch \
  ${CONFIG_LOC} \
  | awk '{ print $NF }')

echo "[STATUS] assigning taxonomy to mags"
PREP_TAX_JOB=$(sbatch \
    -d afterok:"$ANNO_JOB" \
    --wait \
    -o "${LOG_DIR}/slurm-get_mags_for_taxonomy-%A.out" \
    get_mags_for_taxonomy.sbatch \
    ${CONFIG_LOC} \
   | awk '{ print $NF }')

wait

source RUNTIME_VARIABLES.sh

GTDB_JOB=$(sbatch \
  -d afterok:"$PREP_TAX_JOB" \
  --mem=${GTDB_MEM_ALLOC} \
  --cpus-per-task=${GTDB_CPU_ALLOC} \
  -o "${LOG_DIR}/taxonomy/slurm-gtdb_tk_%A.out" \
  gtdb_tk.sbatch \
  ${CONFIG_LOC} \
  | awk '{ print $NF }')

if [ "$KRAKEN_RUN_FLAG" = true  ]; then
 
  KRAKEN_JOB=$(sbatch \
    -d afterok:"$PREP_TAX_JOB" \
    --mem=${KRAKEN_MEM_ALLOC} \
    --cpus-per-task=${KRAKEN_CPU_ALLOC} \
    -o "${LOG_DIR}/taxonomy/slurm-kraken_gtdb202-%A_%a.out" \
    --array=1-"${MAG_COUNT_FOR_TAXONOMY}" \
    kraken2_tax.sbatch \
    ${CONFIG_LOC} \
    | awk '{ print $NF }' )

fi

echo "[STATUS] indexing mags"
MAG_INDEX_JOB=$(sbatch \
  -d afterok:"$ANNO_JOB" \
  --mem=${KALLISTO_MAG_INDEX_MEM_ALLOC} \
  --cpus-per-task=${KALLISTO_MAG_INDEX_CPU_ALLOC} \
  -o "${LOG_DIR}/quantitation/slurm-kallisto_mag_index-%A.out" \
  kallisto_mag_index.sbatch \
  ${CONFIG_LOC} \
  | awk '{ print $NF }' )

echo "[STATUS] quantifying mags in samples"
MAG_QUANT_JOB=$(sbatch \
  -d afterok:"$MAG_INDEX_JOB" \
  --wait \
  --mem=${KALLISTO_MAG_QUANT_MEM_ALLOC} \
  --cpus-per-task=${KALLISTO_MAG_QUANT_CPU_ALLOC} \
  -o "${LOG_DIR}/quantitation/slurm-kallisto_mag_quant-%A_%a.out" \
  --array=1-"${SAMPLES}"%100 \
  kallisto_mag_array.sbatch \
  ${SAMPLES_FILE} \
  ${CONFIG_LOC} \
  | awk '{ print $NF }' )
