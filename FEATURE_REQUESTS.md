# Running To-Do List
- [DEMUX] Look for rates of expected/unexpected UDI pairs, particularly in NovaSeq runs
- [Filtering] Add support for mapping to PhiX in addition to host (mouse, pig, human)
- [Filtering] Add support for phage and eukaryotic assembly/detection in bulk data (Handley)
- [Workflow] Strategies for monitoring >800 parent (and child) jobs and confirming status
- [WORKFLOW] Collision checking based on FILES, not on NAMES from mapping file
- [WORKFLOW] Consider logic for prokka before versus after (as current) drep. Before would allow more flexibility in running drep via multiple strategies.
- [WORKFLOW] Should hybrid assemblies also always include complementary short-read only assemblies?
- [ASSEMBLY] Configure and install metahipmer as an alternative short read assembler

#MCH Simplify seq file naming convention after preprocessing
#NPM Add '--reorder' option to bowtie2-reliant steps to enforce consistent output formatting
#MCH/NPM/HL Run checkm outside dRep to enforce determinism and stable capture of quality metrics
