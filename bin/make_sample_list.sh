#!/bin/sh

if [ -f "${SAMPLES_FILE}" ]; then
    if [ -n "$(grep "^[^#]" ${MAP_FILE} | cut -f 3,4 | sort | uniq | cmp "${SAMPLES_FILE}" - )" ]; then
      echo "[STATUS] Existing ${SAMPLES_FILE} doesn't match. Archiving existing and creating a new SAMPLES.list: ${SAMPLES_FILE}"
      mv "${SAMPLES_FILE}" "${SAMPLES_FILE}.archive"
      grep "^[^#]" ${MAP_FILE} | cut -f 3,4 | sort | uniq > "${SAMPLES_FILE}"
    else
      echo "[STATUS] Validated existing SAMPLES.list: ${SAMPLES_FILE}"
    fi
else
    echo "[STATUS] Creating new SAMPLES.list: ${SAMPLES_FILE}"
    grep "^[^#]" ${MAP_FILE} | cut -f 3,4 | sort | uniq > "${SAMPLES_FILE}"
fi


