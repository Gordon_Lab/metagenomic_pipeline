#!/bin/bash

################################################################################
# SCRIPT NAME : fastp.sbatch.                                                  #
# DESCRIPTION : this script performs quality and adapter trimming              #
# ARGS        : SID.list, config.sh.                                           #
# AUTHOR      : Matthew C. Hibberd                                             #
# EMAIL       : hibberdm@wustl.edu                                             #
# UPDATED     : October 11, 2023                                               #
################################################################################

#SBATCH --mem=1G
#SBATCH --cpus-per-task=2

set -e

if [ -z "${1}" ]; then
    echo "[ERROR] Must pass the SID.list file"
    exit 1
fi

if [ -z "${2}" ]; then
    echo "[ERROR] Must pass the config file"
    exit 1
else
    CONFIG_LOC=$2
fi

source $CONFIG_LOC

VALIDATE=false

METADATA=$( sed -ne '/^#/!p' "${1}" | sed -n "${SLURM_ARRAY_TASK_ID}"p )

IFS=$'\t' read -r RUN_DIR ID SID SUBGROUP PLATFORM HOST <<< "$METADATA"
unset IFS

if [ $PLATFORM = 'novaseq_x_plus' ]; then
	NUM_LANES=8
else
	NUM_LANES=4
fi

mkdir -p  "${QC_DIR}/${RUN_DIR}"

eval $( spack load --sh fastp@0.23.4 )

counter=0

for ((i = 0; i < $NUM_LANES; ++i)); do
    LANE=$(( i + 1 ))

    if [[ -f "${READS_DIR}/${RUN_DIR}/${ID}_${LANE}_R1.fq.gz" &&
    -f "${READS_DIR}/${RUN_DIR}/${ID}_${LANE}_R2.fq.gz" ]]; then

        fastp \
            --in1 "${READS_DIR}/${RUN_DIR}/${ID}_${LANE}_R1.fq.gz" \
            --in2 "${READS_DIR}/${RUN_DIR}/${ID}_${LANE}_R2.fq.gz" \
            --out1 "${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_1.fq.gz" \
            --out2 "${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_2.fq.gz" \
            -h "${QC_DIR}/${RUN_DIR}/${ID}_${LANE}_fastp_report.html" \
            -j "${QC_DIR}/${RUN_DIR}/${ID}_${LANE}_fastp_report.json" \
            --qualified_quality_phred 15 \
            --unqualified_percent_limit 40 \
            --length_required 75 \
            --cut_front \
            --cut_tail \
            --cut_window_size 4 \
            --cut_mean_quality 20 \
            --thread $SLURM_CPUS_PER_TASK 

        counter=$((counter+1))

    else
        echo "${READS_DIR}/${RUN_DIR}/${ID}_${LANE}_R1.fq.gz or \
             ${READS_DIR}/${RUN_DIR}/${ID}_${LANE}_R2.fq.gz not found." 

    fi

        if [ "$VALIDATE" == true ]; then

	    echo '[STATUS] Validating trimmed fastq files'

	    R1_read_ct=`echo $(zcat "${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_1.fq.gz"|wc -l)/4|bc`
	    R2_read_ct=`echo $(zcat "${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_2.fq.gz"|wc -l)/4|bc`

	    if [ "$R1_read_ct" -eq "$R2_read_ct" ]; then
    		echo "[QC passed] The trimmed fastq files have an equal number of reads."
		else
	    	echo "[ERROR] The number of reads in the following two trimmed fastq files do not match: ${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_1.fq.gz, ${QC_DIR}/${RUN_DIR}/${SID}_${LANE}_val_2.fq.gz"
		    exit 1
        fi
	fi


done

if ! ((counter > 0)); then
    echo "[ERROR] No fastq.gz files found for ID ${ID} in ${READS_DIR}/${RUN_DIR}!"
    exit 1
fi


