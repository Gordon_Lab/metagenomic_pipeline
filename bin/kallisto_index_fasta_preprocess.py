#!/usr/bin/env python3

"""Convert a GenBank file into FFN format.

Usage:
    genbank_to_ffn.py <genbank_file>
"""
import sys
import os
import argparse
import textwrap
import gzip

from Bio import SeqIO
from Bio import Seq

def collapse_fasta(fasta_file, input_dir, output_dir):
    base = os.path.splitext(os.path.basename(fasta_file))[0].replace(".fa", "")
    sys.stdout.write("[STATUS]\tGenome: {}\n".format(base))
    
    if fasta_file.endswith(".gz"):
        open_func = gzip.open
        mode = "rt"
    else:
        open_func = open
        mode = "r"

    with open_func(os.path.join(input_dir, fasta_file), mode) as input_handle:
        with open(os.path.join(output_dir, base + "_collapsed.fna"), "w") as output_handle:
            output_handle.write(">{}\n".format(base))
            sequence = str()
            first = True
            for record in SeqIO.parse(input_handle, "fasta"):
                sys.stdout.write("[STATUS]\t\tProcessing contig {}\n".format(record.id))
                if first:
                    sequence = str(record.seq)
                    first = False
                else:
                    sequence += "NNNNNNNNNN" + str(record.seq)
            output_handle.write("{}\n".format(textwrap.fill(sequence, width=60)))

parser = argparse.ArgumentParser(description='Process mash results for kallisto index creation. Run script from within directory containing all genome fasta files.')
parser.add_argument('-d', '--directory', dest="input_dir", default="./", help="How many strains of each species to keep for the quantification step")
parser.add_argument('-o', '--output', dest="output_dir", default="collapsed", help="Directory to put files for kallisto index creation. Default is moving them one directory up.")
parser.add_argument('-e', '--extension', dest="ext", default=".fna", help="File extension to filter on.")
args = parser.parse_args()

if __name__ == "__main__":
    files = os.listdir(args.input_dir)
    if not os.path.exists(args.output_dir):
        os.mkdir(args.output_dir)
    for f in files:
        if f.endswith(args.ext):
            collapse_fasta(f, args.input_dir, args.output_dir)

