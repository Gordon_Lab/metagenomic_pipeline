#!/bin/bash

#Slurm email messages
#SBATCH --mem=4G
#SBATCH --cpus-per-task=4
#SBATCH -o logs/slurm-binning_concoct-%A_%a.out

eval $(spack load --sh py-concoct/trshwc3) #1.1.0

set -e

if [ -z "${1}" ]; then
    echo "[ERROR] Must pass the ASSEMBLIES.list file"
    exit 1
fi

if [ -z "${2}" ]; then
    echo "[ERROR] Must pass the ASSEMBLIES.list"
    exit 1
else
    CONFIG_LOC=$2
fi

source $CONFIG_LOC

BIN_DIR_TOOL="${BIN_DIR}_concoct"
mkdir -p $BIN_DIR_TOOL

ASSEMBLY=$( sed -ne '/^#/!p' "${1}" | sed -n "${SLURM_ARRAY_TASK_ID}"p )

mkdir -p ${BIN_DIR_TOOL}/${ASSEMBLY}

export NUMEXPR_MAX_THREADS=$SLURM_CPUS_PER_TASK

awk 'NR > 1 {for(x=1;x<=NF;x++) if(x == 1 || (x >= 4 && x % 2 == 0)) printf "%s", $x (x == NF || x == (NF-1) ? "\n":"\t")}' ${CONTIG_COUNTS_DIR}/${ASSEMBLY}_kallisto.depth > ${CONTIG_COUNTS_DIR}/${ASSEMBLY}_kallisto.depth.concoct

concoct \
    --composition_file ${ANNO_DIR}/${ASSEMBLY}/${ASSEMBLY}.fna \
    --coverage_file ${CONTIG_COUNTS_DIR}/${ASSEMBLY}_kallisto.depth.concoct \
    -b ${BIN_DIR_TOOL}/${ASSEMBLY}/ \
    -t $SLURM_CPUS_PER_TASK

mkdir -p ${BIN_DIR_TOOL}/${ASSEMBLY/fasta_bins

extract_fasta_bins.py \
    ${ANNO_DIR}/${ASSEMBLY}/${ASSEMBLY}.fna \
    ${BIN_DIR_TOOL}/${ASSEMBLY}/clustering_gt1000.csv \
    --output_path ${BIN_DIR_TOOL}/${ASSEMBLY}/fasta_bins
