#!/bin/bash

################################################################################
# SCRIPT NAME : megahit_array.sbatch                                           #
# DESCRIPTION : this script performs assembly using the megahit assembler      #
# ARGS        : mapping file (positional, optional)                            #
# AUTHOR      : Matthew C. Hibberd                                             #
# EMAIL       : hibberdm@wustl.edu                                             #
# UPDATED     : October 11, 2023                                               #
################################################################################

#Slurm email messages
#SBATCH --mem=1G
#SBATCH --cpus-per-task=2
#SBATCH -o "logs/qc/slurm-estimate_library_insert_size-%A_%a.out"

set -e

if [ -z "${1}" ]; then
    echo "[ERROR] Must pass the config file"
    exit 1
else
    CONFIG_LOC=$1
fi

source $CONFIG_LOC

VALIDATE=false

mkdir -p "${QC_DIR}/library_qc_metrics"

SEQUENCING_RUN=$( sed -ne '/^#/!p' "$SEQ_RUNS_FILE" | sed -n "${SLURM_ARRAY_TASK_ID}"p )

eval $(spack load --sh samtools@1.9/ngs6sq7)

# create a consolidated bam file for each sequencing run
samtools \
  merge \
  --threads $SLURM_CPUS_PER_TASK \
  ${QC_DIR}/library_qc_metrics/${SEQUENCING_RUN}_illumina_hostfilter.bam \
  ${HF_DIR}/${SEQUENCING_RUN}/*illumina_hostfilter.bam

# filter the consolidated bam to remove low confidence alignments and multimapping alignments
samtools \
  view \
  -h \
  -q 42 \
  -O "SAM" \
  --threads $SLURM_CPUS_PER_TASK \
  ${QC_DIR}/library_qc_metrics/${SEQUENCING_RUN}_illumina_hostfilter.bam \
  > ${QC_DIR}/library_qc_metrics/${SEQUENCING_RUN}_illumina_hostfilter_uniqueonly.sam

# filter the sam to include alignments that are paired, properly oriented, and high quality
awk \
  '$2 == 83 || $2 == 147' \
  ${QC_DIR}/library_qc_metrics/${SEQUENCING_RUN}_illumina_hostfilter_uniqueonly.sam \
  > ${QC_DIR}/library_qc_metrics/${SEQUENCING_RUN}_illumina_hostfilter_uniqueonly_properpairs.sam

# extract the insert size metrics from the filtered sam file
cat \
  ${QC_DIR}/library_qc_metrics/${SEQUENCING_RUN}_illumina_hostfilter_uniqueonly_properpairs.sam \
  | cut -f 9 \
  | awk '{print sqrt($0^2)}' \
  > ${QC_DIR}/library_qc_metrics/${SEQUENCING_RUN}_insert_sizes.txt


