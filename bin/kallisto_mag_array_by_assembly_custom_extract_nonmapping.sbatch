#!/usr/bin/env bash

#SBATCH --mem 24G
#SBATCH --cpus-per-task 4
#SBATCH --output logs/quantitation/slurm-kallisto_mag_quant-%A_%a.out

set -e

if [ -z "${1}" ]; then
    echo "[ERROR] Must pass the config file!"
    exit 1
else
    CONFIG_LOC=$1
fi

eval $(spack load --sh kallisto/n52qdt3) #0.48.20

source $CONFIG_LOC

export WORK_DIR=$( pwd )
HF_DIR="${WORK_DIR}/../02_host_filtered"
MAG_COUNTS_DIR="${WORK_DIR}/12_mag_quantification"

METADATA=$( sed -ne '/^#/!p' "SAMPLES.list" | sed -n "${SLURM_ARRAY_TASK_ID}"p )

IFS=$'\t' read -r SID ASSEMBLY <<< "$METADATA"
unset IFS

mkdir -p "${MAG_COUNTS_DIR}/${ASSEMBLY}/kallisto_output"

if [[ -z $SID || -z $ASSEMBLY ]]; then
    echo "[ERROR] Did not recognize either SID or ASSEMBLY! Did you pass the SID.list file?"
    exit 1
fi

INDEX_CHECK=$(find ${MAG_COUNTS_DIR}/${ASSEMBLY}/ -type f -name *.kallisto | wc -l)

if [[ $INDEX_CHECK = 1 ]]; then
  KALLISTO_MAG_INDEX=$( find ${MAG_COUNTS_DIR}/${ASSEMBLY}/ -type f -name *.kallisto )
fi

if [ ! -f ${KALLISTO_MAG_INDEX} ]; then
    echo "[ERROR] kallisto index $KALLISTO_MAG_INDEX not found!"
    exit 1
else
    echo "[STATUS] kallisto index: $KALLISTO_MAG_INDEX"
    echo "[STATUS] SID: $SID"
fi

if [[ -f "${HF_DIR}/concatenated/${SID}_illumina_hostfilter_sort_R1.fastq.gz" && -f "${HF_DIR}/concatenated/${SID}_illumina_hostfilter_sort_R2.fastq.gz" ]]; then

    kallisto \
        quant \
        -b 100 \
        -i ${KALLISTO_MAG_INDEX} \
        -o ${MAG_COUNTS_DIR}/${ASSEMBLY}/kallisto_output/${SID} \
        -t $SLURM_CPUS_PER_TASK \
	--genomebam \
        -g ${MAG_COUNTS_DIR}/../${ASSEMBLY}/${ASSEMBLY}.gtf \
        "${HF_DIR}/concatenated/${SID}_illumina_hostfilter_sort_R1.fastq.gz" \
        "${HF_DIR}/concatenated/${SID}_illumina_hostfilter_sort_R2.fastq.gz"

elif [[ -f "${HF_DIR}/concatenated/${SID}_pacbio_hostfilter_sort_R1.fastq.gz" || -f "${HF_DIR}/concatenated/${SID}_nanoporehostfilter_sort_R1.fastq.gz" ]]; then

     echo "[STATUS] Only long read data was found for sample $SID"

else

    echo "[ERROR] No data found for sample $SID"
    exit 1

fi

