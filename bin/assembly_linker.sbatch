#!/bin/bash

################################################################################
# SCRIPT NAME : megahit_array.sbatch                                           #
# DESCRIPTION : this script performs assembly using the megahit assembler      #
# ARGS        : mapping file (positional, optional)                            #
# AUTHOR      : Matthew C. Hibberd                                             #
# EMAIL       : hibberdm@wustl.edu                                             #
################################################################################

#Slurm email messages
#SBATCH --mem=200M
#SBATCH --cpus-per-task=1

set -e

if [ -z "${1}" ]; then
    echo "Must pass the metadata"
    exit 1
fi

if [ -z "${2}" ]; then
    echo "[ERROR] Must pass the config file"
    exit 1
else
    CONFIG_LOC=$2
fi

source $CONFIG_LOC

mkdir -p ${HF_DIR}/assembly/

METADATA=$( sed -ne '/^#/!p' "${1}" | sed -n "${SLURM_ARRAY_TASK_ID}"p )

IFS=$'\t' read -r SID ASSEMBLY <<< "$METADATA"
unset IFS

##
echo '[STATUS] Assembly: '${ASSEMBLY}
echo '[STATUS] Sample ID: '${SID}

echo -e "Linking files:\n${HF_DIR}/concatenated/${SID}_HF_CAT_R1.fq.gz to ${HF_DIR}/assembly/${ASSEMBLY}_${SID}_HF_CAT_R1.fq.gz"
echo -e "Linking files:\n${HF_DIR}/concatenated/${SID}_HF_CAT_R2.fq.gz to ${HF_DIR}/assembly/${ASSEMBLY}_${SID}_HF_CAT_R2.fq.gz"

ln -sf "${HF_DIR}"/concatenated/"${SID}"_HF_CAT_R1.fq.gz "${HF_DIR}"/assembly/"${ASSEMBLY}_${SID}"_HF_CAT_R1.fq.gz
ln -sf "${HF_DIR}"/concatenated/"${SID}"_HF_CAT_R2.fq.gz "${HF_DIR}"/assembly/"${ASSEMBLY}_${SID}"_HF_CAT_R2.fq.gz
