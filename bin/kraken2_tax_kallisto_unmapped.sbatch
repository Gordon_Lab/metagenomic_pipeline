#!/usr/bin/env bash

################################################################################
# SCRIPT NAME : kraken_tax.sbatch                                              #
# DESCRIPTION : this script performs a taxonomic analysis of sequencing reads  #
# ARGS        : mapping file (positional)                                      #
# AUTHOR      : Matthew C. Hibberd                                             #
# EMAIL       : hibberdm@wustl.edu                                             #
# UPDATE      : October 11, 2013.                                              #
################################################################################

#SBATCH --mem=80G
#SBATCH --cpus-per-task=2
#SBATCH --output logs/quantitation/slurm-kallisto_unmapped-%A_%a.out

set -e

if [ -z "${1}" ]; then
    echo "[ERROR] Must pass the config file"
    exit 1
else
    CONFIG_LOC=$1
fi

source $CONFIG_LOC

export WORK_DIR=$( pwd )
HF_DIR="${WORK_DIR}/../02_host_filtered"
MAG_COUNTS_DIR="${WORK_DIR}/12_mag_quantification"
POST_QC_DIR="${WORK_DIR}/post_quant_qc"

METADATA=$( sed -ne '/^#/!p' "SAMPLES.list" | sed -n "${SLURM_ARRAY_TASK_ID}"p )

IFS=$'\t' read -r SID ASSEMBLY <<< "$METADATA"
unset IFS

eval $(spack load --sh kraken2/a6jag2l ) #2.1.2) 
eval $(spack load --sh bracken@2.8)
eval $(spack load --sh samtools/6iugrna)

export KRAKEN_DB="/ref/jglab/data/kraken/current/"

mkdir -p $POST_QC_DIR
mkdir -p ${POST_QC_DIR}/${ASSEMBLY}

samtools view \
    -f 4 \
    ${MAG_COUNTS_DIR}/${ASSEMBLY}/kallisto_output/${SID}/pseudoalignments.bam \
    | samtools sort \
        -n \
        --output-fmt SAM - \
        | samtools fastq \
            -1 ${MAG_COUNTS_DIR}/${ASSEMBLY}/kallisto_output/${SID}/${SID}_unmapped_R1.fastq \
            -2 ${MAG_COUNTS_DIR}/${ASSEMBLY}/kallisto_output/${SID}/${SID}_unmapped_R2.fastq

kraken2 \
    --db $KRAKEN_DB \
    --threads $SLURM_CPUS_PER_TASK \
    --report "${POST_QC_DIR}/${ASSEMBLY}/${SID}.k2report" \
    --use-names \
    --paired \
    ${MAG_COUNTS_DIR}/${ASSEMBLY}/kallisto_output/${SID}/${SID}_unmapped_R1.fastq \
    ${MAG_COUNTS_DIR}/${ASSEMBLY}/kallisto_output/${SID}/${SID}_unmapped_R2.fastq \
    --confidence 0.1 \
    --output "${POST_QC_DIR}/${ASSEMBLY}/${SID}.kraken2"

bracken \
    -d $BRACKEN_DB \
    -i "${POST_QC_DIR}/${ASSEMBLY}/${SID}.k2report" \
    -o  "${POST_QC_DIR}/${ASSEMBLY}/${SID}.bracken"
