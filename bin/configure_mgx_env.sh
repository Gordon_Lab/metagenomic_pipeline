#!/bin/bash

## SET DEFAULTS
WORK_DIR=$( pwd )
CONFIG_LOC=$( which config.sh )
if [ -f "${WORK_DIR}/config.sh" ]; then
    CONFIG_LOC="${WORK_DIR}/config.sh"
fi
#echo "[CONFIG FILE] $CONFIG_LOC"
source $CONFIG_LOC

## MAPPING FILE QC
# Check that the mapping file exists
if [ -z "${MAP_FILE}" ]; then
    echo "ERROR: Must specify the mapping file in config.sh!"
    exit 1
#else
#    echo "[MAPPING FILE] $MAP_FILE"
fi

# Convert newlines from dos to unix format (if necessary)
dos2unix -ic ${MAP_FILE} | xargs dos2unix

# This command makes sure a newline exists at the end of the mapping file.
sed -i -e '$a\' ${MAP_FILE}

## JOB ARRAY HANDLING
# This command determines the number of samples from the mapping file for use in
# creating job arrays.
#SEQ_FILE_PAIRS=$( sed -ne '/^#/!p' "${MAP_FILE}" | wc -l )

# Process the mapping file to generate a nonredundant list of samples
sh make_sample_list.sh

# Process the mapping file to generate a list of assemblies
sh make_assembly_list.sh

FLOW_CELLS_X_SAMPLES=$( sed -ne '/^#/!p' $MAP_FILE | wc -l )

SAMPLES=$( wc -l "${WORK_DIR}/SAMPLES.list" | cut -d " " -f 1 )

ASSEMBLIES=$( wc -l "${WORK_DIR}/ASSEMBLIES.list" | cut -d " " -f 1 )

echo "[STATUS] config file: $CONFIG_LOC"
echo "[STATUS] mapping file: ${WORK_DIR}/$MAP_FILE"
echo "[STATUS] SAMPLES.list: ${WORK_DIR}/SAMPLES.list ($SAMPLES total samples)"
echo "[STATUS] ASSEMBLIES.list: ${WORK_DIR}/ASSEMBLIES.list ($ASSEMBLIES total assemblies)"

