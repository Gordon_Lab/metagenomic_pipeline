#!/bin/sh

DIR=$( pwd )

step1=$( which metagenomic_assembly_pipeline.sbatch )
cp $step1 $DIR

step2=$( which mag_assembly_pipeline.sbatch )
cp $step2 $DIR

step3=$( which README.md )
cp $step3 $DIR

step4=$( which example_mapping_file.txt )
cp $step4 $DIR

step5=$( which config.sh )
cp $step5 $DIR
